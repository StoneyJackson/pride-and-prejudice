# Pride and Pejudice - The Workflow Activity

The book in pride-and-prejudice.txt is in the public domain. It was
downloaded from Project Gutenberg. However, since this activity will
modify this work, as per the Project Gutenberg License, it is no longer
associated with the Project Gutenberg. The derivative work, and the
other files in this project, are licensed under GPLv3.

## Prepare the activity

```bash
$ python scramblewords.py < pride-and-prejudice.txt > pride-and-prejudice-scrambled.txt
$ mv pride-and-prejudice-scrambled.txt pride-and-prejudice.txt
$ git add .
$ git commit -m "Scramble"
$ git push
```

